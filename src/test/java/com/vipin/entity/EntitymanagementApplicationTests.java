package com.vipin.entity;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;

import com.vipin.entity.model.Person;
import com.vipin.entity.model.PersonList;


@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
class EntitymanagementApplicationTests {

	@Autowired
    private TestRestTemplate restTemplate;
	
	@Test
	void contextLoads() {
	}

    @LocalServerPort
    int randomServerPort;
	
    @Test
    public void testPersonApi() throws URISyntaxException
    {
    	System.out.println("Test running with port : " + randomServerPort);
        final String baseUrl = "http://localhost:"+randomServerPort+"/api/v1/persons";
        URI uri = new URI(baseUrl);
        Person person = new Person("Vipin", "Purohit", 30, "Violet");
         
        restTemplate.getRestTemplate().getInterceptors().add(
        		  new BasicAuthorizationInterceptor("admin", "password"));
        
        /*
         * 1. Test for add Person
         */

        HttpHeaders headers = new HttpHeaders();
        
        HttpEntity<Person> request = new HttpEntity<>(person, headers);
         
        ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
         
        //Verify request succeed
        Assert.assertEquals(201, result.getStatusCodeValue());
        
        
        /*
         * 2. Test to get All Person
         */
        PersonList response = restTemplate.getForObject(baseUrl, PersonList.class);
        Assert.assertTrue(response.getPerson().size() == 1);
     
        
        /*
         * 3. Test to update Person with id 1
         */
        Person newPerson = new Person("Virat", "Kohli", 33, "Yellow");
        HttpEntity<Person> updteRequest = new HttpEntity<>(newPerson, headers);
         
        this.restTemplate.put(new URI(baseUrl + "/1"), updteRequest);
        PersonList responseAfterUpdate = restTemplate.getForObject(baseUrl, PersonList.class);
        System.out.println("Person after Update: " + responseAfterUpdate.getPerson().get(0));
        Assert.assertTrue(responseAfterUpdate.getPerson().get(0).getFirstName().equals("Virat"));
           
        
        /*
         * 4. Test to delete Person with id 1
         */
        // deleting only Person available with person id 1
        restTemplate. delete(baseUrl + "/1");
        response = restTemplate.getForObject(baseUrl, PersonList.class);
        // after delete size should be 0
        Assert.assertTrue(response.getPerson().size() == 0);

        
    }
	
	
}
