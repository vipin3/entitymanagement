package com.vipin.entity.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vipin.entity.model.Person;

public interface PersonRepository extends JpaRepository<Person, Integer> {

}
