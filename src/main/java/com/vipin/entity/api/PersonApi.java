package com.vipin.entity.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vipin.entity.dao.PersonRepository;
import com.vipin.entity.model.Person;
import com.vipin.entity.model.PersonList;

@RestController
@RequestMapping(value = "/api/v1")
public class PersonApi {

	@Autowired
	private PersonRepository personRepository;

	/*
	 * Add new Person
	 */
	@PostMapping(value = "/persons")
	public ResponseEntity<Object> addPerson(@RequestBody Person person) {
		personRepository.save(person);
		return new ResponseEntity<>("Person is created successfully", HttpStatus.CREATED);
	}

	/*
	 * Get a Person by id
	 */
	@GetMapping(value = "/persons/{id}")
	public ResponseEntity<Object> get(@PathVariable("id") String id) {
		
		Optional<Person> personData = personRepository.findById(Integer.parseInt(id));
		if (personData.isPresent()) {
			Person existingPerson = personData.get();
			return new ResponseEntity<>(existingPerson, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Person not found", HttpStatus.NOT_FOUND);
		}
	}
	
	/*
	 * Get all Person
	 */
	@GetMapping(value = "/persons")
	public PersonList list() {
		List<Person> person = personRepository.findAll();
		PersonList list = new PersonList();
		list.setPerson(person);
		return list;
	}

	/*
	 * Delete a person by id
	 */
	@DeleteMapping(value = "/persons/{id}")
	public ResponseEntity<Object> deletePerson(@PathVariable(name = "id") String id) {
		try {
			personRepository.deleteById(Integer.parseInt(id));
			return new ResponseEntity<>("Person is deleted successfully", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("Person not found", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/*
	 * Update a person by id
	 */
	@PutMapping(value = "/persons/{id}")
	public ResponseEntity<Object> updatePerson(@RequestBody Person person, @PathVariable(name = "id") String id) {
		Optional<Person> personData = personRepository.findById(Integer.parseInt(id));
		if (personData.isPresent()) {
			Person existingPerson = personData.get();
			existingPerson.setFirstName(person.getFirstName());
			existingPerson.setLastName(person.getLastName());
			existingPerson.setAge(person.getAge());
			existingPerson.setFavouriteColour(person.getFavouriteColour());
			return new ResponseEntity<>(personRepository.save(existingPerson), HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Person not found", HttpStatus.NOT_FOUND);
		}

	}

}
