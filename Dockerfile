FROM openjdk:11
ADD target/docker-rest-spring-boot.jar docker-rest-spring-boot.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-rest-spring-boot.jar"]