Get the code:
-------------------
Clone the repository:
     
	 $ git clone https://vipin3@bitbucket.org/vipin3/entitymanagement.git


To build the application:
-------------------	
From the command line:

	$ cd entitymanagement
	$ mvn clean install


Run the application from Spring boot 
-------------------

       $ mvn spring-boot:run


Run the application using Docker
-------------------

Run a public Docker image of this app which is exposing REST API.

docker run -p 8085:8085 vipinpurohit/docker-rest-spring-boot:latest


Using the API
-------------------

Use Basic Auth as Authorization to call all below APIs, Where Username=admin and Password=password. Any REST Client like POSTMAN can be used to test them.


1. POST		http://localhost:8085/api/v1/persons		
	Add a Person

	REQUEST:

	{
	    	"first_name": "Vipin",
		"last_name": "Purohit",
    		"age": 30,
    		"favourite_colour": "Green"
	}

		Here {Id} is an integer value like 1.

2. GET		http://localhost:8085/api/v1/persons		
	Retrieve all Person

3. GET		http://localhost:8085/api/v1/persons/{Id}	
	Retrieve a Person by id

4. PUT		http://localhost:8085/api/v1/persons/{Id}	
	Update a Person by id
		
	REQUEST:
		
	{
	    	"first_name": "VipinNew",
		"last_name": "PurohitNew",
    		"age": 40,
    		"favourite_colour": "Yellow"
	}

		Here {Id} is an integer value like 1.


5. DELETE	http://localhost:8085/api/v1/persons/{Id}	
	Delete a Person by id
	
		Here {Id} is an integer value like 1.

